var express = require('express');
var router = express.Router();
const low = require('lowdb')
const storage = require('lowdb/file-sync')
const trackerdb = low('db/tracker.json', { storage })

/* get records for this id */
router.get('/tracker/:id', function(req,res,next){
  records = trackerdb('records')
    .chain()
    .filter({id: req.params.id})
    .value();

  res.send(records);

});
/* monitor permits to collect GPS data through online monitor option. */
/* client provides http://example.com/apis/tracker/valvin?lat=a&lon=b&timestamp=c&hdop=d&altitude=e&speed=f */
/* this is not REST compliant but easy to setup in OSMAnd add-on */
router.get('/tracker/:id/add', function(req, res, next) {
  id = req.params.id;
  timestamp = req.query.timestamp;
  lat = req.query.lat;
  lon = req.query.lon;
  altitude = req.query.altitude;
  speed = req.query.speed;
  hdop = req.query.hdop;
  if(timestamp && lat && lon && altitude && speed && hdop){
    record = {
      id: id,
      timestamp: timestamp,
      lat: lat,
      lon: lon,
      altitude: altitude,
      speed: speed,
      accuracy: hdop
    };
    trackerdb('records').push(record);
    console.log(timestamp + ' : new record for '+ id + ': accuracy : ' + hdop + ' - '  + lat + '/' + lon + '/' + altitude + ' - ' + speed );
    res.send({
      message: 'ok',
      record: record}
    );
  }
  else{
    //FIXME : Find the good error code
    res.status(404);
    res.send({message: 'malformed query : ' + JSON.stringify(req.query)});
  }

});

module.exports = router;
