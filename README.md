## OSM-Traffic (OSMT)
The goal of this project is to offer a GPS car system based on OpenStreetMap which in addition to standard navigation functionnalities offers traffic.
The navigation is calculated depending the traffic and drivers can gain lot of time.
In order to evaluate traffic OSMt will work from :
* open data stream like Bison-Futée in France
* user collected data with privacy consideration
The secondary goal is to offer user the possibility to indicate some alerts (traffic, accident, risks ...)

So OSM-Traffic wants to be an opensource alternative to Waze / TomTom Traffic ...

